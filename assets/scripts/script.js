// API
const apiUrl = 'https://api.spacexdata.com/v3/launches';

// params
const defaultList = '?limit=100';
const launchParam = '&launch_success=';
const landParam = '&land_success=';
const yearParam = '&launch_year=';

// filte types obj
const filterTypes = {
  YEAR : 'year',
  LAUNCH : 'launch',
  LAND : 'land',
}

// params data
let launchSuccess = '';
let landSuccess = '';
let launchYear = '';

// element selected
let loaderGif;
let cardBlock;
let cardMessage;

// get elements
(() => {
  loaderGif = document.getElementById('loader');
  cardBlock = document.getElementById('flex__row--card');
  cardMessage = document.getElementById('not__found');
})();

// years list
const yearsArr = ["2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"];
const yearList = () => {
  const btnTem = document.getElementById('filters__years');
  yearsArr.map(year => {
    const btnTemplate = document.getElementById('btn__year').cloneNode(true);
    btnTemplate.removeAttribute('id');
    btnTemplate.innerHTML = year;
    btnTemplate.setAttribute('data-filter' , year);
    btnTemplate.setAttribute('data-filter-type' , 'year');
    btnTemplate.addEventListener('click', selectedFilter);
    btnTem.append(btnTemplate);
  });
};

// launch list
const launchArr = [{ 'launch' : true }, { 'launch' : false }];
const launchList = () => {
  const btnTem = document.getElementById('filters__launch');
  launchArr.map((val) => {
    const btnTemplate = document.getElementById('btn__launch').cloneNode(true);
    btnTemplate.removeAttribute('id');
    btnTemplate.innerHTML = val.launch;
    btnTemplate.setAttribute('data-filter' , val.launch);
    btnTemplate.setAttribute('data-filter-type' , Object.keys(val)[0]);
    btnTemplate.addEventListener('click', selectedFilter);
    btnTem.append(btnTemplate);
  });
};

// land list
const landArr = [{ 'land' : true }, { 'land' : false }];
const landList = () => {
  const btnTem = document.getElementById('filters__landing');
  landArr.map(val => {
    const btnTemplate = document.getElementById('btn__land').cloneNode(true);
    btnTemplate.removeAttribute('id');
    btnTemplate.innerHTML = val.land;
    btnTemplate.setAttribute('data-filter' , val.land);
    btnTemplate.setAttribute('data-filter-type' , Object.keys(val)[0]);
    btnTemplate.addEventListener('click', selectedFilter);
    btnTem.append(btnTemplate);
  });
};

// loader
const loader = () => {
  loaderGif.classList.remove('loader--hide');
  cardBlock.classList.add('card--hide');
};

// spacex card list api call
const spacexLists =  async (params = '') => {
  loader();
  let removeEle = document.querySelectorAll('.space__cards--main');
  const cardTem = document.getElementById('flex__row--card');
  let setUrlParam = params != '' ? `${defaultList}${params}` : `${defaultList}`;
  window.history.pushState(location.search, '', `${setUrlParam}`);
  let respose = await fetch(`${apiUrl}${defaultList}${params}`);
  let dataList = await respose.json();

  if (removeEle.length !== 0) {
    for (let index = 0; index < removeEle.length; index++) {
      removeEle[index].remove();
    } 
  }

  loaderGif.classList.add('loader--hide');
  cardBlock.classList.remove('card--hide');

  if (dataList.length !== 0 ) {
    cardMessage.classList.add('not__found--hide');
  } else {
    cardMessage.classList.remove('not__found--hide');
  }

  dataList.map((data) => {
    const cardTemplate = document.getElementById('space__cards').cloneNode(true);
    cardTemplate.removeAttribute('id');
    cardTemplate.classList.add('space__cards--main');
    let imgUrl = data.links.mission_patch_small != null ? data.links.mission_patch_small : ''
    cardTemplate.children[0].children[0].setAttribute('src', imgUrl);
    cardTemplate.children[0].children[0].setAttribute('alt', data.mission_name);
    cardTemplate.children[1].innerText = ` ${data.mission_name} #${data.flight_number}`;
    cardTemplate.children[2].children[1].innerText = ` ${data.mission_id}`;
    cardTemplate.children[3].children[1].innerText = ` ${data.launch_year}`;
    cardTemplate.children[4].children[1].innerText = ` ${data.launch_success}`;
    cardTemplate.children[5].children[1].innerText = ` ${data.rocket.first_stage.cores[0].land_success}`;
    cardTem.append(cardTemplate);
  });
};

// filter btn status
const selectedFilter = (e) => {
  let filterVaue = e.target.getAttribute('data-filter');
  let filterType = e.target.getAttribute('data-filter-type');
  let filterTypeBtnList = document.querySelectorAll(`.btn--${filterType}`);
  for (let index = 0; index < filterTypeBtnList.length; index++) {
    let current = filterTypeBtnList[index].getAttribute('data-filter');
    if(filterVaue !== current) {
      filterTypeBtnList[index].classList.remove('active');
    }
  }
  if (filterType === filterTypes.YEAR) {
    launchYear = e.target.classList.contains('active') ? '' : filterVaue;
  } else if (filterType === filterTypes.LAUNCH) {
    launchSuccess = e.target.classList.contains('active') ? '' : filterVaue;
  } else if (filterType === filterTypes.LAND) {
    landSuccess = e.target.classList.contains('active') ? '' : filterVaue;
  }
  filterParamUrl();
  e.target.classList.toggle("active");
};

// setting url params
const filterParamUrl = () => {
  let newLaunch = launchSuccess !== '' ? `${launchParam}${launchSuccess}` : '';
  let newLand = landSuccess !== '' ? `${landParam}${landSuccess}` : '';
  let newYear = launchYear !== '' ? `${yearParam}${launchYear}` : '';
  let newfilter = `${newLaunch}${newLand}${newYear}`;
  spacexLists(newfilter)
}

// onload 
window.addEventListener('load', () => {
  spacexLists();
  yearList();
  launchList();
  landList();
});